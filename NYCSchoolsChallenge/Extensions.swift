//
//  Extensions.swift
//  NYCSchoolsChallenge
//
//  Created by Ayyappa ch on 2/21/24.
//


import SwiftUI

extension Color {
    static let publicNavy = Color("publicNavy"),
               nycGreen = Color("nycGreen"),
               schoolOrange = Color("schoolOrange"),
               boardBlue = Color("boardBlue")
}
