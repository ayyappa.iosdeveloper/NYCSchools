//
//  NYCSchoolsChallengeApp.swift
//  NYCSchoolsChallenge
//
//  Created by Ayyappa ch on 2/21/24.
//


import SwiftUI

@main
struct NYCSchoolsChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListScreen()
        }
    }
}

