//
//  SchoolListViewModelSuccessTests.swift
//  NYCSchoolsTests
//
//  Created by Ayyappa ch on 2/21/24.
//

import XCTest
@testable import NYCSchoolsChallenge

final class SchoolListViewModelSuccessTests: XCTestCase {
    func testSuccessfulLoadSchoolList() async {
        let networkingMock: any NetworkingManagerImpl = NetworkingManagerSchoolResponseSuccessMock()
        let viewModel = SchoolListViewModel(networkingManager: networkingMock)
        XCTAssertTrue(viewModel.schools.isEmpty)
        await viewModel.fetchSchools()
        XCTAssertFalse(viewModel.schools.isEmpty)
    }
}

